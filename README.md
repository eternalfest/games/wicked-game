# Wicked Game

Clone with HTTPS:
```shell
git clone https://gitlab.com/eternalfest/games/wicked-game.git
```

Clone with SSH:
```shell
git clone git@gitlab.com:eternalfest/games/wicked-game.git
```
