package wicked;

import hf.mode.GameMode;
import merlin.IAction;
import merlin.IActionContext;

class ResetLevel implements IAction {
    public var name(default, null): String;
    public var isVerbose(default, null): Bool = false;

    public function new(name: String) {
        this.name = name;
    }

    public function run(ctx: IActionContext): Bool {
        var game: GameMode = ctx.getGame();

        game.clearLevel();
        game.cleanKills();
        game.world.scriptEngine.insertBads();
        game.fl_badsCallback = false;

        return false;
    }
}
