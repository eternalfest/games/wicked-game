package wicked;

import etwin.Obfu;
import merlin.IAction;

@:build(patchman.Build.di())
class Wicked {

    @:diExport
    public var resetLevel(default, null): IAction;

    public function new() {
        resetLevel = new ResetLevel(Obfu.raw("resetLevel"));
    }
}
