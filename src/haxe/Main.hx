import bugfix.Bugfix;
import debug.Debug;
import hf.Hf;
import merlin.Merlin;
import game_params.GameParams;
import patchman.IPatch;
import patchman.Patchman;
import better_script.NoNextLevel;
import atlas.Atlas;
import wicked.Wicked;

@:build(patchman.Build.di())
class Main {
  public static function main(): Void {
    Patchman.bootstrap(Main);
  }

  public function new(
      bugfix: Bugfix,
      debug: Debug,
      gameParams: GameParams,
      noNextLevel: NoNextLevel,
      atlasNinjutsu: atlas.props.Ninjutsu,
      atlas: atlas.Atlas,
      wicked: Wicked,
      merlin: Merlin,
      patches: Array<IPatch>,
      hf: Hf
  ) {
    Patchman.patchAll(patches, hf);
  }
}
